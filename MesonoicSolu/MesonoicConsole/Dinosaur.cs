﻿using System;

namespace Mesozoic
{
    public class Dinosaur
    {
        private string name;
        private string specie;
        private int age;


        public Dinosaur(string name, string specie, int age)
        {
            this.name = name;
            this.specie = specie;
            this.age = age;
        }
        
        public string sayHello()
        {
            return string.Format("Je suis {0} le {1}, j'ai {2} ans.",this.name, this.specie,this.age);
        }

        public string roar()
        {
            return "Grrr";
        }

        public string getName()
        {
            return this.name;
        }

        public string getSpecie()
        {
            return this.specie;
        }

        public int getAge()
        {
            return this.age;
        }

        public void getName(string name)
        {
            this.name = name;
        }
        public void getSpecie(string specie)
        {
            this.specie = specie;
        }
        public void getAge(string age)
        {
            this.age = age;
        }
    }
}