﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mesozoic;

namespace MesoTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestDinosaurConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

            Assert.AreEqual("Louis", louis.getName());
            Assert.AreEqual("Stegausaurus", louis.getSpecie());
            Assert.AreEqual(12, louis.getAge());
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);

            Assert.AreEqual("Nessie", nessie.getName());
            Assert.AreEqual("Diplodocus", nessie.getSpecie());
            Assert.AreEqual(11, nessie.getAge());
        }

        [TestMethod]
        public void TestDinosaurRoar()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.AreEqual("Grrr", louis.roar());

            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Assert.AreEqual("Grrr", nessie.roar());
        }

        [TestMethod]
        public void TestDinosaurSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.AreEqual("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.sayHello());

            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Assert.AreEqual("Je suis Nessie le Diplodocus, j'ai 11 ans.", nessie.sayHello());
        }
    }
}
